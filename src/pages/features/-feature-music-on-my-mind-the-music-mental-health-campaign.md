---
templateKey: review-post
path: /feature-music-on-my-mind
title: Music on my Mind
date: 2019-08-10T14:00:00.000Z
description: >-
  Our campaign to raise funds and awareness of mental health issues in the
  public AND music community
featuredImage: /img/momm-webage.jpg
---
Get tickets to MUSIC ON MY MIND: RAVE FOR MENTAL HEALTH [**HERE**](https://moshtix.com.au/v2/event/music-on-my-mind-rave-for-mental-health/115142)

After the success of our campaign to raise funds and awareness for mental health issues in the public and music community last year, MUSIC ON MY MIND is back! 

In 2018, with a grant from City of Sydney and the support of Sydney's electronic music industry, we raised a total of $18,750 for mental health charities Support Act, beyondblue and Black Dog Institute through our fundraising rave and online appeal. Our social media campaign on mental health also reached over 250,000 people, helping to raise awareness. 

This year, in partnership with [Ticketswap](ticketswap.com.au), we're planning to make Music on my Mind even bigger and better. 
<br><br>

**BACKGROUND**

Around 45 percent of people in Australia will experience a mental health condition in their lifetime. In any one year, approximately 1 million Australian adults have depression, and over 2 million have anxiety. Rates are even higher in musicians, who are almost seven times as likely to have thought about suicide in the past 12 months in comparison to the general population, and are twice as likely to attempt to take their own lives.

Some of the factors that contribute to these high rates include erratic sleep patterns, performance anxiety, hectic touring schedules, easy access to alcohol and drugs, uncertain income and lack of recognition - or for the very successful; the intense scrutiny that comes with fame.

Mental health is an issue close to our hearts at Rave Reviewz, since our founder Dr Kamran Ahmed is a psychiatrist who became aware of the high rates of mental health problems in musicians when he started working in the music industry. So we often ask about mental health when we interview DJs, but decided that we should do more.

The mental health benefits of music are well known; people listen to music to regulate their mood, and music therapy is used as part of the treatment for depression, autism, schizophrenia, dementia, agitation, anxiety, sleeplessness and substance misuse.

Music is also a great way to raise awareness of mental health issues, to tackle the stigma that surrounds them and to raise money so charities can continue their great work. That's why we launched our music and mental health campaign - ‘MUSIC ON MY MIND’ and we hope you can support the cause.
<br><br>

**MUSIC ON MY MIND 2019**

All proceeds from the campaign will be going to these superstar charities...

[Support Act](https://supportact.org.au/) - helps artists and music workers who are facing hardship due to illness, a mental health problem, injury or some other crisis.

[ReachOut](https://www.facebook.com/ReachOutAUS/) - Australia’s leading online mental health organisation for young people and their parents, helping young people get through anything from everyday issues to tough times with their practical support, tools and tips.
<br><br>

**Music on my Mind: Rave for Mental Health**

![null](/img/momm2019.jpg)

We're teaming up with the legends at Subsonic Music, S.A.S.H, Blueprint and WeLove for our monster dress-up fundraising rave on Sat 21st Sept at Oxford Art Factory, featuring B2B sets from some of Sydney’s biggest DJs. Details and tickets [here](https://www.facebook.com/events/446674325919617/).

Big thanks to our event partners:  Ticketswap, Moshtix, Keep Sydney Open, Electronic music conference, Paul Strange presents. 

Best outfit on the night wins a FREE TICKET to Subsonic Music Festival! We had some mad outfits last year and can’t wait to see what you come up with this time, so get creative to win. 

And you can look forward to next level visuals supplied by Eyebyte on the night.

This will be another unforgettable night, so grab your friends and get your tickets [**HERE**](https://moshtix.com.au/v2/event/music-on-my-mind-rave-for-mental-health/115142) now!
<br><br>

**THE AWARENESS CAMPAIGN**

Throughout October, we’ll be raising awareness of issues related to mental health and music and tackling the stigma that surrounds mental health conditions. 

Keep an eye on our [Facebook page](https://www.facebook.com/ravereviewz/) for our daily posts on music and mental health.
<br><br>

**MUSIC ON MY MIND 2018**

Here's a little recap of last year's Music on my Mind: Rave for Mental Health

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fravereviewz%2Fvideos%2F359731594802468%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

![null](/img/event-image.png)

**2018 SUPPORTERS**

Afters / ALLFRIENDS / Attic / Bare Essentials / Bassic Records / Bermuda Club / Bizarre Haberdashery / Borough / By Your Side / Canvas / Church of Techno / City of Sydney / Compass Events / Cruise Control / Days Like This / Decoded Magazine / Deep House Yoga Project / Deep Seahorse Podcast / Deeper Than House / Division Agency / Don't Kill My Vibe / Dopamine / Dresscode / Earthlings Collective / Echo Music Australia / Eclipse Music /  Electronic Music Conference / Friday Records / IDWT / Ignite / Keep Sydney Open / Kiltr Sessions / Let The People Dance / Momentum / Monos / Move for Mood / Motif / MusicNSW / Norti Vikings / NSW Health / PACE / Party Merchants / Paul Strange Presents / Porridge / Roots / SASH / Social Bewegung / Something Else / Spektrum / Standout Media / Summerlove / Summit DJs / Sutra / Sydney's Eyes / The Studio / T1000 events / This / Xposed Media / Zoo 
<br><br>
